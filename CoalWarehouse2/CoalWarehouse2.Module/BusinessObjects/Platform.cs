﻿using System.Linq;
using DevExpress.Xpo;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using System;
using DevExpress.Data.ODataLinq.Helpers;

namespace CoalWarehouse2.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Platform : XPObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Platform(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        private string _platformNumber;
        private int _capasity;
        private string _description;
        private WareHouses _warehouseNumber;

        [Size(7)]
        [XafDisplayName("Нумерация пикетов площадки")]
        public string PlatformNumber
        {
            get
            {
                return GetPicketsName();
            }
            //только чтение
            private set
            {
                SetPropertyValue("PlatformNumber", ref _platformNumber, value);
            }
        }

        //получение всех имен пикетов, относящихся к платформе
        private string GetPicketsName()
        {
            string picketsNames;
            //обработка проблемы подключения к БД, либо отсуствия пикета в Платформе
            try
            {
                List<string> listPickets = new List<string>();

                foreach (var pickets in Pickets)
                {
                    listPickets.Add(pickets.PicketNumber);
                }
                picketsNames = string.Join(", ", listPickets);
                if (picketsNames == "")
                    picketsNames = "Пикеты не привязаны";
            }
            catch
            {
                picketsNames = "Данные о пикетах не получены";
            }
            return picketsNames;
        }

        [XafDisplayName("Груз на площадке")]
        public int Capasity
        {
            get
            {
                return _capasity;
            }
            set
            {
                SetPropertyValue("Capasity", ref _capasity, value);            
            }
        }

        [Size(2048)]
        [XafDisplayName("Описание")]
        public string Description
        {
            get 
            { 
                return _description; 
            }
            set 
            { 
                SetPropertyValue("Description", ref _description, value); 
            }
        }

        [XafDisplayName("Склад")]
        public WareHouses WarehouseNumber
        {
            get
            {
                return _warehouseNumber;
            }
            set
            {
                SetPropertyValue("WarehouseNumber", ref _warehouseNumber, value);
            }
        }
        //выборка из неиспользуемых пикетов

        [Association("Platform-Pickets")]
        public XPCollection<Picket> Pickets
        {
            get { 
                return GetSelectedPickets(Session); 
            }
        }
        private XPCollection<Picket> GetSelectedPickets(Session session)
        {
            IQueryable<Picket> pickets;
            List<Picket> selectedPickets;
            XPCollection<Picket> collection = new XPCollection<Picket>(session); ;
            if (!IsSaving && !IsLoading)
            {
                pickets = session.Query<Picket>().Where(w => w.Platform != null)/*.Where(w => w.Platform.WarehouseNumber == w.WarehouseNumber)*/;
                selectedPickets = pickets.ToList();

                collection.LoadingEnabled = false;
                foreach (var p in pickets)
                {
                    collection.Add(p);
                }
            }        
            return collection;
        }
    }
}