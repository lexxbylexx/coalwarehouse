﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CoalWarehouse2.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Picket : XPObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Picket(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
     
        private string _picketNumber;
        private WareHouses _warehouseNumber;
        private Platform _platform;
        [Size(3)]
        [XafDisplayName("Номер пикета"), RuleRequiredField]
        public string PicketNumber
        {
            get
            {
                return _picketNumber;
            }
            set
            {
                SetPropertyValue("PicketNumber", ref _picketNumber, value);
            }
        }
        [Size(10)]
        [XafDisplayName("Название склада"), RuleRequiredField]
        public WareHouses WarehouseNumber
        {
            get
            {
                return _warehouseNumber;
            }
            set
            {
                SetPropertyValue("WarehouseNumber", ref _warehouseNumber, value);
            }
        }

        //площадка в пикетах как "один ко многим"        
        [Association("Platform-Pickets")]
        public Platform  Platform
        {
            get => _platform;
            set => SetPropertyValue(nameof(Platform), ref _platform, value);
        }

        //выводим пикеты площадки
  /*      [XafDisplayName("Площадка")]
        public string PlatformNumber 
          {
              get
              {
                if (Platform.PlatformNumber != null)
                {
                    return Platform.PlatformNumber;
                }
                else return "Нет пикетов";
              }
          } */
    }
    public enum WareHouses
    {
        WareHouse1,
        WareHouse2
    }
}